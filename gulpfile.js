var gulp = require('gulp'),
	copy = require('gulp-copy'),
	merge = require('merge-stream'),
	replace = require('gulp-replace');

gulp.task('copy', function() {
	var bower = gulp.src([
		'bower_components/**/*.*'
	]).pipe(copy('demo-site/', {prefex: 1}));

	var thisComponent = gulp.src([
		'./**/*.*',
		'!./bower_components/**',
		'!./node_modules/**'
	]).pipe(copy('demo-site/bower_components/now-left-right2/'));

	return merge(bower,thisComponent);
});

gulp.task('updateDocs', ['copy'], function() {
	var replaceWebComp = "../webcomponentsjs/webcomponents-lite.js";
	var withWebComp = "../../webcomponentsjs/webcomponents-lite.js";

	var replaceCompPage = "../iron-component-page/iron-component-page.html";
	var withCompPage = "../../iron-component-page/iron-component-page.html"

	var replaceCompTag = "<iron-component-page></iron-component-page>";
	var withCompTag = "<iron-component-page src='../../now-left-right2/now-left-right2.html' base='../../now-left-right2/'></iron-component-page>"

	return gulp.src('demo-site/bower_components/now-left-right2/index.html')
		.pipe(replace(replaceWebComp, withWebComp))
		.pipe(replace(replaceCompPage, withCompPage))
		.pipe(replace(replaceCompTag, withCompTag))
		.pipe(gulp.dest('demo-site'));
});

gulp.task('deploy', ['copy']);